class Image:
    """
    image_id
    tag_number
    tags
    """

    def __init__(self, image_id, tag_number, tags=None):
        self.image_id = image_id
        self.tag_number = tag_number
        self.tags = tags

    def __repr__(self):
        return "ID: " + str(self.image_id) + " TAG_NR: " + str(self.tag_number) + " TAGS: " + str(self.tags)


class Horizontal(Image):
    def __init__(self, image_id, tag_number, tags=None):
        super(Horizontal, self).__init__(image_id, tag_number, tags)


class Vertical(Image):
    def __init__(self, image_id, tag_number, tags=None):
        super(Vertical, self).__init__(image_id, tag_number, tags)


class Slide:

    def __init__(self, image: Horizontal = None, image1: Vertical = None, image2: Vertical = None):
        if image is not None:
            self.slide_id = image.image_id
            self.tag_number = image.tag_number
            self.tags = image.tags
        else:
            self.slide_id = str(image1.image_id) + " " + str(image2.image_id)
            self.tag_number = tags_common([image1, image2])
            self.tags = set(image1.tags + image2.tags)

    def __repr__(self):
        return str(self.slide_id) + "\n"


def tags_common(arr):
    a = arr[0]
    b = arr[1]
    ret_value = 0
    for ta in a.tags:
        for tb in b.tags:
            if ta == tb:
                ret_value += 1
    return ret_value


if __name__ == "__main__":
    ficheros = ["a_example.txt", "b_lovely_landscapes.txt", "c_memorable_moments.txt", "d_pet_pictures.txt",
                "e_shiny_selfies.txt"]

    for nombre_fichero in ficheros:
        vertical_images = []
        horizontal_images = []
        f = open(nombre_fichero)
        n = int(f.readline())
        #print(n)
        for i in range(n):
            img = f.readline().split()
            if img[0] == "H":
                horizontal_images.append(Horizontal(i, int(img[1]), img[2:]))
            else:
                vertical_images.append(Vertical(i, int(img[1]), img[2:]))

        f.close()

        """
        print(len(vertical_images))
        print(vertical_images[0])
        print(len(horizontal_images))
        print(horizontal_images[0])
        """

        slides = []
        for i in horizontal_images:
            slides.append(Slide(image=i))

        #FIXME REVISAR
        min_tags_common = []

#        for i in range(len(vertical_images)):
 #           if i %2 == 0:
  #              min_tags_common.append([vertical_images[i], vertical_images[i+1]])

        max_coincidences = 0

        while len(vertical_images) != 0:
            i = 0
            vertical_size = len(vertical_images)
            while i < vertical_size:
                j = i+1
                found = False
                while (j < vertical_size) & (not found):
                    if tags_common([vertical_images[i], vertical_images[j]]) == max_coincidences:
                        min_tags_common.append([vertical_images[i], vertical_images[j]])
                        for e in [i, j]:
                            if e == 0 & e == len(vertical_images) - 1:
                                vertical_images = []
                            elif e == 0:
                                vertical_images = vertical_images[e + 1:]
                            elif e == len(vertical_images) - 1:
                                vertical_images = vertical_images[:e-1]
                            else:
                                vertical_images = vertical_images[:e - 1] + vertical_images[e + 1:]
                        found = True
                    else:
                        j += 1
                if found:
                    vertical_size = len(vertical_images)
                else:
                    i += 1
            max_coincidences += 1

        """
        sorted_array = sorted(min_tags_common, key=tags_common)
        print(len(sorted_array))
        print(sorted_array[0], tags_common(sorted_array[0]))
        """

        for i in min_tags_common:
            slides.append(Slide(image1=i[0], image2=i[1]))

        fich_final = open("output_" + nombre_fichero, "w")
        fich_final.write(str(len(slides)) + "\n")
        for i in slides:
            fich_final.write(i.__repr__())

        fich_final.close()